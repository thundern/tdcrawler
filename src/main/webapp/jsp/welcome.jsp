<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>WELCOME</title>
</head>
<body>
  <center>WELCOME</center>

    <c:if test="${not empty m}">
        id : ${m.id} <br>
        name : ${m.name} <br>
        url : ${m.url} <br>
    </c:if>

  <c:choose>
      <c:when test="${empty m}">
          m is empty or null.
      </c:when>
      <c:otherwise>
          id : ${m.id} <br>
          name : ${m.name} <br>
          url : ${m.url} <br>
      </c:otherwise>
  </c:choose>

  <c:choose>
      <c:when test="${empty m2}">
          m2 is empty or null.
      </c:when>
      <c:otherwise>
          id2 : ${m2.id} <br>
          name2 : ${m2.name} <br>
          url2 : ${m2.url} <br>
      </c:otherwise>
  </c:choose>




</body>
</html>
