<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="/commons/config.jsp"%>

<!-- css link here -->
<!--[if IE 6]>
  <script type="text/javascript" src="${domain_js}/scripts/DD_belatedPNG_0.0.8a-min.js?${version_js}" ></script>
  <script type="text/javascript" src="${domain_js}/scripts/fixpng.js?${version_js}" ></script>
<![endif]-->
<link rel="shortcut icon" href="${domain_image}/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/main.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/customer.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/myaccount.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/product_list.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/template_spot.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/sartorialist.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_css}/css/staticstl.css?${version_css}" />
<link rel="stylesheet" type="text/css" href="${domain_image}/statics/assets/homepage/css/hp-template.css?${version_css}">
<%-- <link rel="stylesheet" type="text/css" href="${domain_css}/css/top5_men_and_women.css?${version_css}" /> --%>
<link rel="stylesheet" type="text/css" href="${domain_css}/css/nav.css?${version_css}" />

<!-- script link here -->
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/template/jquery.min.js?${version_js}"></script>
<script language="JavaScript" type="text/javaScript" src="${domain_js}/scripts/jquery.tools.min.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/jquery.loxia.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/jquery.masonry.min.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/template/hp-includes.min.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/swfobject.js?${version_js}"></script>
<%-- <script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/imgReady.js?${version_js}"></script> --%>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/lazy_load.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript">
	var $j = jQuery.noConflict();
</script>

<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/main.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/customer.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/myaccount.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/product_list.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/product/register_and_login.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/assets/sartorialist.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/nav.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/shopcart/shop_cart.js?${version_js}"></script>
<%-- <script type="text/javascript" src="${domain_js}/scripts/aos/login_webservice.js?${version_js}"></script>
<script type="text/javascript" src="${domain_js}/scripts/aos/register_webservice.js?${version_js}"></script> remove aos 11.3--%>
<script type="text/javascript" src="${domain_js}/scripts/aos/json2.js?${version_js}"></script>

<%-- Omniture code --%>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/s_code.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/CoachChina.OmnitureCustomLinkFunctions.js?${version_js}"></script>
<script language="JavaScript" type="text/javascript" src="${domain_js}/scripts/header_and_footer_omniture.js?${version_js}"></script>
<script type="text/javascript">
// 设置通用的Omniture变量值
eVar8 = '${loginEmail}';
eVar9 = '${loginEmail}' ? 'Registered User' : 'guest';
eVar10 = '${loginEmail}' ? 'Signed-in User' : 'guest';
</script>
<script type="text/javascript">
  var categoryCode = "${categoryCode}";
  var collection = "${collection}";
</script>
