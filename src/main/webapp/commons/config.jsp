<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- css url 版本号 --%>
<c:set var="version_css" value="20141128.01" scope="application"></c:set>
<%-- js url 版本号 --%>
<c:set var="version_js" value="20141128.01" scope="application"></c:set>

<%-- Monthly Image 版本号 每月更新一次 --%>
<c:set var="version_monthly" value="2014.112801" scope="application"></c:set>

<%-- 状态,brandsite只有 首页 搜索 分类 ,明细  ,stroelocal,help  其他没有  brandsite test--%>
<c:set var="system_status" value="brandsite" scope="application"></c:set>

<script type="text/javascript">
	
<%-- 用于拼接 图片路径 --%>
	var domain_image = "${domain_image}";

<%-- 用于拼接连接 contextPath 应用程序名 --%>
	var _contextPath = "${pageContext.request.contextPath}";

<%-- 用于拼接连接 contextPath 应用程序名 --%>
	var _ua = "${header["User-Agent"]}";
</script>

<%-- 有效期的配置 --%>
<%@ include file="/commons/timeConfig.jsp"%>
