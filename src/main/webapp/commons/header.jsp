<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript" src="${domain_js}/scripts/navcookie.js?${version_js}"  charset="UTF-8"></script>
<script type="text/javascript" src="${domain_js}/scripts/MSClass.js?${version_js}"  charset="UTF-8"></script>


<!--header-->
<!--topnav-->
<div class="topnav">
    <div class="branding">
         <div id="navad" class="shipping_free">
           <jsp:useBean id="currTime" class="java.util.Date" />
           <fmt:formatDate value="${currTime}"  pattern="yyyy-MM-dd HH:mm:ss" var="currForMatTime"></fmt:formatDate>
           <c:forEach var="header_txt" items="${header_pdptxt}">
               <fmt:formatDate value="${header_txt.startTime}" pattern="yyyy-MM-dd HH:mm:ss" var="startTime"></fmt:formatDate>
               <fmt:formatDate value="${header_txt.endTime}" pattern="yyyy-MM-dd HH:mm:ss" var="endTime"></fmt:formatDate>
		        <c:if test="${'0' eq header_txt.isHeader and (currForMatTime ge startTime) and (currForMatTime le endTime)}">
		           <a href="${not empty header_txt.remark ? header_txt.remark : 'javascript:void(0);'}" style="cursor:${not empty header_txt.remark ? 'pointer' : 'default'}"><c:out value="${header_txt.content}"/></a>
		        </c:if>
		   </c:forEach>
		 </div>
         <div class="searchbar">
         	<%-- searcher --%>
          	<jsp:include page="/commons/_header_searcher.jsp"></jsp:include>
          	
          	<%-- minicart --%>
          	<dl class="minicart">
				<dt class="shoppingbg" ><a href="${pageContext.request.contextPath}/shoppingCart.htm">购物篮（<span id="headerShopcartNum">0</span>）<span class="dropminicart" style="top:10px;"></span></a></dt>
				<dd id="miniCartBox" class="bagdropdown">
					<jsp:include page="/shopcart/header_cart_list.jsp"></jsp:include>
				</dd>
			</dl>
            
            <%-- myaccount --%>
            <jsp:include page="/commons/_header_myaccount.jsp"></jsp:include>
         </div>
    </div>
</div>
<div class="nav">
    <div id="logo">
      	<a href="${pageContext.request.contextPath}/" title="COACH官方网站,COACH"></a>
      	<!-- 百度快照  seo优化  搜索引擎抓取图片 -->
    </div>
    <ul class="navmenu">
    	<c:forEach var="level1Nav" items="${fullNav}">
    		<%-- location: 1:top, 3:top and left --%>
    		<c:if test="${level1Nav.selfNav.location eq '1' or level1Nav.selfNav.location eq '3'}">
    		<%--  <c:if test="${'051' eq level1Nav.selfNav.code}"">class='snoopy'</c:if> --%>
    		<c:choose>
    		<c:when test="${'062' eq level1Nav.selfNav.code}">
    		<li class="gifts">
    		</c:when>
    		<%-- <c:when test="${'061' eq level1Nav.selfNav.code}">
    		<li class="snoopy">
    		</c:when> --%>
    		<c:when test="${'060' eq level1Nav.selfNav.code}">
    		<li class="storeLocator">
    		</c:when>
    		<c:otherwise>
    		<li>
    		</c:otherwise>
    		</c:choose>
	    		
	    			<%-- 有些一级导航有特殊样式 --%>
	    			<%-- <a <c:if test="${'087' eq level1Nav.selfNav.code}">style='color:#d63a1b;'</c:if> href="${pageContext.request.contextPath}${level1Nav.selfNav.url}${level1Nav.selfNav.requestParams}">${level1Nav.selfNav.name}</a> --%>
	    			<c:choose>
	    			<%-- 61 snoopy ,62 gifts --%>
	    			<%-- <c:when test="${'061' eq level1Nav.selfNav.code}">
	    			<a href="${pageContext.request.contextPath}${level1Nav.selfNav.url}${level1Nav.selfNav.requestParams}" class="headernavbg" title="peanuts"><img width="100%" alt="peanuts" src="${domain_image}/images/nav/snoopynav-transparent.png" ></a>
	    			</c:when> --%>
	    			
	    			<c:when test="${'062' eq level1Nav.selfNav.code}">
	    			<a href="${pageContext.request.contextPath}${level1Nav.selfNav.url}${level1Nav.selfNav.requestParams}" class="headernavbg2" title="gifts">节日礼物</a>
	    			</c:when>
	    			
	    			<c:otherwise>
	    			<a href="${pageContext.request.contextPath}${level1Nav.selfNav.url}${level1Nav.selfNav.requestParams}">
						${level1Nav.selfNav.name}</a> 
	    			</c:otherwise>
	    			</c:choose>
					    			
		    		<c:choose>
		    			<%-- 010:全新商品, 020:女士, 030:男士, 095:礼物推荐, 080:网上独家商品, 090:热卖商品, 100:特色专题, 110:各地专卖店, 140:节日礼物 --%>
		    			<c:when test="${fn:contains('050@020@030@080@090@095@100@110@140@062', level1Nav.selfNav.code)}">
		    				<c:if test="${level1Nav.hasChildren}">
		    					<dl>
		    						<c:forEach var="level2Nav" items="${level1Nav.childNavs}">
		    							<c:if test="${level2Nav.selfNav.location eq '1' or level2Nav.selfNav.location eq '3'}">
			    							<c:choose>
			    								<c:when test="${(empty level2Nav.selfNav.url) and (level2Nav.selfNav.name eq 'BLANK')}">
			    									<dd><h4 class="highlight"></h4></dd>
			    								</c:when>
			    								<c:when test="${(empty level2Nav.selfNav.url)}">
			    									<dd><h4 class="highlight">${level2Nav.selfNav.name}</h4></dd>
			    								</c:when>
			    								<c:otherwise>
			    									<dd><a href="${pageContext.request.contextPath}${level2Nav.selfNav.url}${level2Nav.selfNav.requestParams}">${level2Nav.selfNav.name}</a></dd>    
			    								</c:otherwise>
			    							</c:choose>
			    							<%-- 全新商品和礼物推荐导航显示到三级 --%>
			    							<c:if test = "${(level1Nav.selfNav.code eq '010' or level1Nav.selfNav.code eq '095' or level1Nav.selfNav.code eq '140' or level2Nav.selfNav.code eq '020150' or level2Nav.selfNav.code eq '030300') and (level2Nav.hasChildren)}">
			    								<c:forEach var="level3Nav" items="${level2Nav.childNavs}">
			    									<c:if test="${level3Nav.selfNav.location eq '1' or level3Nav.selfNav.location eq '3'}">
			    										<dd><a href="${pageContext.request.contextPath}${level3Nav.selfNav.url}${level3Nav.selfNav.requestParams}">${level3Nav.selfNav.name}</a></dd>  
			    									</c:if>
			    								</c:forEach>
			    							</c:if>
		    							</c:if>
		    						</c:forEach>
		    					</dl>
		    				</c:if>
		    			</c:when>
		    			
		    			
		    			<%-- 040:手袋, 045:服饰, 050:鞋类, 060:配饰 --%>
		    			<c:when test="${(level1Nav.selfNav.code eq '040') or (level1Nav.selfNav.code eq '045')  or (level1Nav.selfNav.code eq '060')}">
		    				<c:if test="${level1Nav.hasChildren}">
		    					<ol>
		    						<%-- 左 --%>
		    						<c:if test="${fn:length(level1Nav.childNavs) > 0}">
		    							<div class="leftColumnDrop">
		    								<li><h4 class="highlight">女士</h4></li>
		    								<c:forEach var="level3Nav" items="${level1Nav.childNavs[0].childNavs}">
		    									<c:if test="${level3Nav.selfNav.location eq '1' or level3Nav.selfNav.location eq '3'}">
					    							<c:choose>
					    								<c:when test="${(empty level3Nav.selfNav.url) and (level3Nav.selfNav.name eq 'BLANK')}">
					    									<li><h4 class="highlight"></h4></li>
					    								</c:when>
					    								<c:when test="${(empty level3Nav.selfNav.url)}">
					    									<li><h4 class="highlight">${level3Nav.selfNav.name}</h4></li>
					    								</c:when>
					    								<c:otherwise>
					    									<li><a href="${pageContext.request.contextPath}${level3Nav.selfNav.url}${level3Nav.selfNav.requestParams}">${level3Nav.selfNav.name}</a></li>    
					    								</c:otherwise>
					    							</c:choose>
				    							</c:if>
				    						</c:forEach>
		    							</div>
		    						</c:if>
		    						<%-- 右 --%>
		    						<c:if test="${fn:length(level1Nav.childNavs) > 1}">
		    							<div class="rightColumnDrop">
		    								<li><h4 class="highlight">男士</h4></li>
		    								<c:forEach var="level3Nav" items="${level1Nav.childNavs[1].childNavs}">
		    									<c:if test="${level3Nav.selfNav.location eq '1' or level3Nav.selfNav.location eq '3'}">
					    							<c:choose>
					    								<c:when test="${(empty level3Nav.selfNav.url) and (level3Nav.selfNav.name eq 'BLANK')}">
					    									<li><h4 class="highlight"></h4></li>
					    								</c:when>
					    								<c:when test="${(empty level3Nav.selfNav.url)}">
					    									<li><h4 class="highlight">${level3Nav.selfNav.name}</h4></li>
					    								</c:when>
					    								<c:otherwise>
					    									<li><a href="${pageContext.request.contextPath}${level3Nav.selfNav.url}${level3Nav.selfNav.requestParams}">${level3Nav.selfNav.name}</a></li>    
					    								</c:otherwise>
					    							</c:choose>
				    							</c:if>
				    						</c:forEach>
		    							</div>
		    						</c:if>
		    					</ol>
		    				</c:if>
		    			</c:when>
		    			
		    			<c:otherwise>
		    			</c:otherwise>
		    		</c:choose>
	    		</li>
    		</c:if>
    	</c:forEach>
    	
    </ul>
</div>
<!--topnav end-->

<c:if test="${empty sessionScope['session.user.context'].member or sessionScope['session.user.context'].memberId eq 1}">
	<%@ include file="/member/member_login.jsp"%>
</c:if>
<jsp:include page="../member/member_register.jsp"></jsp:include>

<!--header_end-->
<script type="text/javascript">
	shopCart.requestHeaderShoppingCart();
	var pathname = window.location.pathname;
	if(pathname.indexOf("shoppingCart.htm")!=-1){
		shopCart.requestShoppingCart();
	}

	new Marquee("navad",0,4,700,40,30,5000,500);

</script>
<!--[if IE 6]>

<script type="text/javascript" src="${domain_js}/scripts/DD_belatedPNG_0.0.8a-min.js?${version_js}" ></script>

<script type="text/javascript" src="${domain_js}/scripts/fixpng.js?${version_js}" ></script>

<![endif]-->

	<!--make_sure_detail-->
<div id="makesure_pop_box" class="makesure_pop" style="display:none;">
	<div class="payfor_title" id="makesure_pop_title"></div>
	<div class="payfor_content">
    	<a href="###" class="btnlink" id="surebtn">确定</a>
    	<a href="###" class="btnlink" id="cancelbtn">取消</a>
    </div>
</div>



