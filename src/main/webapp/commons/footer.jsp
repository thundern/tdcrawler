<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="${domain_js}/css/footer.css?${version_css}" rel="stylesheet" type="text/css" />
<script type="text/JavaScript" src="${domain_js}/scripts/member/member_login.js?${version_js}"></script>
<script type="text/JavaScript" src="${domain_js}/scripts/footer.js?${version_js}"></script>

<!-- footer -->
<div id="coach_footer">
	<div class="footersupplementary">
	<div id="coach_footer_super">
		
			<!--ABOUT COACH & LOGO-->
			<div id="about_coach">
				<p class="font1">关于COACH</p>
			</div>
			<div class="footerclear"></div>
			
			<!--MENU-->
			<div id="footer_menu">
				<div>
					<ul style="padding-top: 10px; !important;" class="font3">
						<li><a href="${pageContext.request.contextPath}/storelocator.htm">各地专卖店</a></li>
						<li><a href="${pageContext.request.contextPath}/helpcenter/webside-nav.htm">网站导航</a></li>
						<li><a href="http://www.coach.com/international/" target="_blank">COACH全球网站</a></li>
					</ul>
				</div>
				<div>
					<ul style="padding-top: 10px; !important;" class="font3">
						<li><span>400-001-7021</span></li>
						<li><a href="${pageContext.request.contextPath}/helpcenter/ten-reason.htm">网上购物的十大理由</a></li>
						<li><a href="${pageContext.request.contextPath}/helpcenter/shopping-help.htm">在线购物帮助</a></li>
						<li><a href="${pageContext.request.contextPath}/helpcenter/customer-service.htm">客户服务</a></li>
                        <li id="footerlive114958"></li>
					</ul>
				</div>
				<div>
					<ul style="padding-top: 10px; !important;" class="font3">
						<li><a href="${pageContext.request.contextPath}/static/timeline.htm">COACH历史传承</a></li>
						<li><a href="${pageContext.request.contextPath}/static/heritage.htm">COACH传承视频</a></li>
						<li><a href="${pageContext.request.contextPath}/helpcenter/company-profileUS.htm">公司信息</a></li>						
						<li><a href="${pageContext.request.contextPath}/helpcenter/job-wanted.htm">求职</a></li>
						<li><a href="http://ishine.coach.com" target="_blank">店铺招聘</a></li>
					</ul>
				</div>
				<div>
					<ul style="padding-top: 10px; !important;" class="font3">
						<li><a href="${pageContext.request.contextPath}/helpcenter/private-policy.htm">隐私政策</a></li>
						<li><a href="http://www.miibeian.gov.cn">沪ICP备09069012</a></li>
                        <li><a target="_blank" href="http://www.sgs.gov.cn/lz/licenseLink.do?method=licenceView&entyId=20120411001332964"><img src="${domain_image}/images/footer/shanghaiGSicon.gif" /></a></li>
					</ul>
				</div>
			</div>
			
			<!--EMAIL SIGNUP & SOCIAL SHARING-->
			<div id="footer_social_wrapper">
				<div id="footer_social">
					<div id="footer_signup">
						<ul class="coach_connect" id="joinme">
							<li class="font1">获取COACH新讯</li>
							<li>
								<input type="text" value="输入电子邮件" size="30" name="email_signup" id="email_signup" onfocus="if(this.value=='输入电子邮件')this.value='';" onblur="if(this.value=='')this.value='输入电子邮件';" title="Signup" maxlength="64" >
								<input type="submit" onclick="javascript:onlyEmail();" value="加入我们" id="signup_btn">
							</li>
						</ul>
					</div>
					<div id="footer_share">
					  <ul class="coach_connect">
							<li class="font1" style="display: inline;">关注COACH</li>
							<li style="display: inline;">
								<img usemap="#social_links" src="${domain_image}/images/footer/connecticon.png?${version_css}" style="border: none; vertical-align: top; height: 17px; margin-top: -2px !important;">
							    <map name="social_links" id="social_links">
							         <area shape="rect" coords="5,0,27,13" href="http://e.weibo.com/coachchina?ref=http%3A%2F%2Fchina.coach.com%2Fonline%2Fhandbags%2FFrontDoorOneView%3FstoreId%3D14001%26catalogId%3D14500%26langId%3D-1" target="_blank"/>
							         <!-- <area shape="rect" coords="38,0,60,13" href="http://site.douban.com/coachchina/" target="_blank"/> -->
							    </map>
							</li>
							<li style="display: inline;"><div data-show-faces="false" data-width="450" data-layout="button_count" data-send="false" data-href="http://www.facebook.com/coach" class="fb-like"></div></li>
						</ul>
					</div>
				</div>
			</div>
            
            <!-- copyright -->
            <div id="footer_copy">				
            	<p class="font4">©2014 COACH, INC.、 COACH®、COACH标志C的设计、COACH欧普图案的设计、COACH标签、COACH菱形图案以及COACH马车图案的设计均为COACH的注册商标。</p>			
            </div>		
	</div>
	</div>
</div>
<!--footer end-->
<div class="backtotop"></div>
<!--列表页商品名-->
<span id="ruler" style="visibility: hidden; white-space: nowrap;"></span>
<!--login_-->
   <div class="footer_login" style=" display:none;">
   	<div class="footer_login_title">
			加入我们
			<img class="black_cross" src="${domain_image}/images/main/black_cha.jpg">
	</div>
	<div class="footer_login_left">
		<img class="register_leftimg" src="${domain_image}/images/delivery/email_signup.jpg?${version_css}" style=" width:208px; height:258px;">
	</div>
	<div class="footer_login_right">
		<h1>谢谢您</h1>
		<p>欢迎加入我们，您将第一时间了解激动人心的新品发布、特别活动、门店开张、及更多资讯。</p>
		<p> 为了更好服务您，希望您完整的填写您的基本信息，让我们能及时的通知您COACH的最新资讯。</p>
		<a href="###" id="join_me" >继续完成注册 >> </a>
	</div>
	<div class="footerclear"></div>
   </div>
   <script>
   $j(document).ready(function(){
      $j(".footer_login_title").click(function(){
   	   //$j(".black_opacity").fadeOut(15);
     	$j(".footer_login").fadeOut(15);
     });
      //截断列表页  商品名
      /* if($j("#pagegrid")[0]) {
    	 	 $j("#pagegrid #_name").each(function() {
    	  	    if($j(this).parent().find("#_price").offset().top>=$j(this).offset().top+40) {
    	  	    	$j("#ruler").html($j(this).text()+$j(this).parent().find("#_price").text());//innerWidth   
    	  	    	var rout =  $j("#ruler")[0].offsetWidth/$j(this)[0].offsetWidth;
    	  	    	var len = Math.round($j("#ruler")[0].offsetWidth - rout*$j(this)[0].offsetWidth);//$j(this).text()+$j(this).parent().find("#_price").text()-2*$j(this)[0].offsetWidth;
    	  	     	$j(this).text($j(this).text().substring(0,$j(this).text().length-13-len)+"...");
    	  	    }
    	  	 });
      } */
   });
   </script>
   <!--login__end-->
