<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:parseDate value="${'2014-06-12 00:00:00'}"  pattern="yyyy-MM-dd HH:mm:ss" scope="application" var="startActivityTime"></fmt:parseDate> 
<fmt:parseDate value="${'2014-06-15 23:59:59'}"  pattern="yyyy-MM-dd HH:mm:ss"  scope="application" var="endActivityTime"></fmt:parseDate>
