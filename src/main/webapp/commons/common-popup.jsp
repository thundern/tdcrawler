<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- 
<script type="text/javascript">
function  Add_to_my_favorite_Register()
{
	var click1 = new Image();
	var axel = Math.random() + "";
	var a = axel * 10000000000000;
	click1.src ='https://3890374.fls.doubleclick.net/activityi;src=3890374;type=coach261;cat=addto546;ord=' + a + '?';
}

function jump()
{
	Add_to_my_trolley_Check_out();
	window.location.href ="${pageContext.request.contextPath}/shoppingCart.htm";
	
}
	
function  Add_to_my_trolley_Check_out()
{	
	var click1 = new Image();
	var axel = Math.random() + "";
	var a = axel * 10000000000000;
	click1.src ='https://3890374.fls.doubleclick.net/activityi;src=3890374;type=coach261;cat=addto714;ord=' + a + '?';
}

function  Add_to_my_trolley_continue_shopping()
{
	var click1 = new Image();
	var axel = Math.random() + "";
	var a = axel * 10000000000000;
	click1.src ='https://3890374.fls.doubleclick.net/activityi;src=3890374;type=coach261;cat=addto876;ord=' + a + '?';
}
</script>
 -->
<div class="black_opacity"></div>

<!--product_explain-->
<div class="product_explain">
	 <em class="arrow"></em>
     <p class="product_explain_title"></p>
     <p class="bold product_explain_price"></p>
</div>
<!--product_explain_end-->

<!--common_pop_up-->
<div class="dialog_all" id="_common_popup">
     <div class="dialog_common">
     	  <img src="${domain_image}/images/closebtn_01.jpg" class="black_cross" />
          <div class="dialog_common_content">
          	  提示信息
          </div>
     </div>
</div>
<!--common_pop_up_end-->

<!--product_detail-->
<!-- 
<div class="product_detail">
	<img class="black_cross" src="${domain_image}/images/closebtn_01.jpg" style="width:17px; height:17px; top:5px; right:5px;">
	<span class="info">商品详情加载中...</span>
</div>
 -->
<!--product_detail_end-->

<!--also like pop-->
<div id="popup-allbox" class="dialog_all pop_all" style="z-index: 12;">
    <img class="black_cross imgclose" src="${domain_image}/images/closebtn_01.jpg" />
    <div id="also_like_popup">
        <div class="also_like_content">
            <ul>
            	
            </ul>
            <div class="clear"></div>
        </div>
        <div class="also_like_back"><a href="javascript:;"><img src="${domain_image}/images/ArrowLeft.gif" style="vertical-align:top;" alt="" />返回产品详情</a></div>
    </div>
</div>
<!--also like pop end-->

<!--make_sure_detail-->
<div id="makesure_pop_box" class="makesure_pop" style="display:none;">
	<div class="payfor_title" id="makesure_pop_title"></div>
	<div class="payfor_content">
    	<a href="javascript:;" class="btnlink" id="surebtn">确定</a>
    	<a href="javascript:;" class="btnlink" id="cancelbtn">取消</a>
    </div>
</div>
<!--make_sure_detail_end-->

<!--错误提示-->
<div id="checkErrorBox" class="checkoutError">
	<div class="popupTitle"><span class="tt">请注意以下信息</span><span id="errorClose" class="closelink">X</span><div class="clear"></div></div>
    <div class="popupContent">
    	<ul id="errorInfoList"></ul>
    </div>
</div>
<!--错误提示-->

<!--largeimage view-->
<div id="view_large" class="dialog_all pop_all" style="z-index: 12;">
	<img class="black_cross imgclose" src="${domain_image}/images/closebtn_01.jpg" />
    <div id="view_main">
    	<div id="_large_flv" style="width: 650px;height: 415px;">
	    	load large image...
		</div>
        <div class="clear"></div>
        <div class="also_like_back"><a href="javascript:;"><img src="${domain_image}/images/ArrowLeft.gif" style="vertical-align:top;" alt="" />返回产品详情</a></div>
    </div>
</div>
<!--largeimage view end-->

<!--register_login_dialog-->
<div class="dialog_all" id="wishlist_dia">
	<input type="hidden" value="" id="d_callBack"/>
	 <div class="dialog_content">
          <img src="${domain_image}/images/main/black_cha.jpg" class="d_black_cross" />
          
          <div class="dialog_title">我的收藏夹</div>
          <div class="dialog_word">将您最喜爱的商品存在收藏夹中</div>
          
          <div class="dislog_advance" style="height:566px;"><img src="${domain_image}/images/product_list/70488_mah_a0.jpg"/></div>
          
          <div class="wishlist_dia_square" id="d_register" style="height:380px;">
               <label>注册新账户并开始设置您的收藏夹</label>
               <p>您可以将喜爱的商品添加至您的收藏夹。</p>
               <p>称谓</p>
               <div class="dia_reg_newline" style="margin-right:20px;"><input type="radio" id="gender" name="gender" value="女"/>女士</div>
               <div class="dia_reg_newline"><input type="radio" id="" name="gender" value="男" />先生</div>
               <div class="dia_reg_line" style="margin-right:20px;"><span>姓氏</span><input type="text" id="d_firstName" maxlength="10"/></div>
               <div class="dia_reg_line"><span>名字</span><input type="text" id="d_lastName" maxlength="10"/></div>
               <div class="dia_reg_line" style="margin-right:20px;"><span>电子邮件</span><input type="text" id="d_email" maxlength="50" checkurl="${pageContext.request.contextPath}/member/memberemailcheck.json"/></div>
               <div class="dia_reg_line"><span>国家</span><select class="w130" style="width:160px;height:18px;border:1px solid #cccccc;" id="country_r"></select></div>
               <div class="dia_reg_line" style="width:55px;"><span>国家号码</span><input value="" type="text" class="text" style="width:50px;" id="iddCode_r" readonly="readonly"/></div>
               <div class="dia_reg_line" style="width:10px;"><span style="color:#999999;width:10px;float:left;margin-top:19px;">-</span></div>
               <div class="dia_reg_line" style="width:105px;"><span>手机号码</span><input type="text" class="text" style="width:95px;" id="d_mobile" checkurl="${pageContext.request.contextPath}/member/membermobilecheck.json"/></div>
               <div class="clear"></div>
               
               <div class="dia_reg_line" style="margin-right:20px;"><span>密码</span><input type="password" id="d_register-password" maxlength="50" autocomplete="off"/></div>
               <div class="dia_reg_line"><span>确认密码</span><input type="password" name="passwordAgain" id="d_passwordAgain" maxlength="50" autocomplete="off"/></div>
              
               <p>(需至少7个字符，包含至少一个数字及字母)</p>
               <p><input type="checkbox" checked="checked" style="margin-right:5px; vertical-align:middle;" id="d_isBookEmail" value="true"/>我希望收到COACH相关信息。</p>
               <div class="mb6" style="margin-top:5px;">
            		<input type="checkbox" style="vertical-align:middle; margin-right:5px;float:left;" id="agreementChecked" checked="checked"/>
            		<div style="width:236px;height:45px;color:#000000;font-size:12px;border:1px solid #666666;overflow-y:auto;padding:0 5px;">我同意以上资料将会用作COACH市场及推广之用。为作此等用途，COACH可能会将我的个人资料收集、存储、披露或传送至关联公司或其他有关机构，或与之共享资料。</div>
           	   </div>
               <input type="button" class="black_btn" id="d_regist_btn" value="注册并继续" />
          </div>
          
          <div class="wishlist_dia_square" id="d_login" style="height:136px; margin-top:10px;">
               <label>我已有自己的COACH账户</label>
               
                <div class="clear"></div>
               
               <div class="dia_reg_line" style="margin-right:20px;"><span>电子邮件</span><input type="text"  id="d_loginName" maxlength="50" name="loginName"/></div>
               <div class="dia_reg_line"><span>密码</span><input type="password" id="d_password" name="password" maxlength="50"/></div>
              
               <p><a href="javascript:void(0);" class="forget_pwd1">忘记了您的密码?</a></p>
               <p style="width:180px">* 当我们将新密码发送到您的邮箱并登录后请将喜爱的商品添加至收藏夹。</p>
               <input type="button" id="d_login_btn" class="black_btn" value="登录并继续" />
          </div>
     </div>
</div>
<!--register_login_dialog_end-->
<!-- 注册成功 -->
<div id="register_succeed" class="dialog_all">
	<div class="register_title">注册成功<img src="${domain_image}/images/dialog/dialog-close.jpg" class="register_close"></div>
	<img src="${domain_image}/images/delivery/email_confirm.jpg?${version_css}" alt="" />
	<a class="register_myact" href="${pageContext.request.contextPath}/memberaccount.htm">我的账户</a>
	<script>
		function showRegisterSucceed(){
			var scrlps=$j(window).scrollTop();
			//$j(".black_opacity").fadeIn(200);
			$j(".dialog_all").hide();
			$j("#register_succeed").fadeIn(200);
			$j("#register_succeed .register_close").click(function(){
				//$j(".black_opacity").fadeOut(15);
				$j("#register_succeed").fadeOut(15);
                //window.location.reload();
				var url = window.location.href;
				if(url.indexOf("?register") > 0)
					url = url.replace("register","");
				window.location.href = clearHttpsParams(url.replace(/#+$/,""));
			});
		}
	</script>
</div>
<!-- 注册成功结束 -->

<!--动画pop-->
<div id="DivContainer">
	<a href="javascript:;" id="closeAnimatePop"><img src="${domain_image}/images/closebtn_01.jpg" /></a>
    <iframe id="animate_frame" frameborder="0" src="" id="animate_frame" name="animate_frame" scrolling="no"></iframe>
</div>
<!--end-->

<!-- 你可能会喜欢 -->
<!--you_may_also_like-->
<div class="dialog_all fav_success" id="you_may_also_like_product_detail">
	<div class="like_left_proimg">
    	<h3 class="like_title">您可能会喜欢...</h3>
        <div id="you_may_also_like_div"></div>
    </div>
    <div class="like_right_info">
    	<h3>购物篮</h3>
        <div class="like_info_box">
            <h4><span id="skuNameProductDetail"></span></h4>
            <p>价格：￥<span id="skuPriceProductDetail"></span></p>
            <p>颜色：<span id="skuColorProductDetail"></span></p>
            <p id="isShowSizeProductDetail">尺码：<span id="skuSizeProductDetail"></span></p>
            <p>数量：<span id="skuNumProductDetail"></span></p>
         </div>
        <h3>您的商品已成功添加至购物篮</h3>
        <p align="right" class="like_ck_btn" style="padding-top:10px;">                      
            <a href="${pageContext.request.contextPath}/shoppingCart.htm">结账</a>
        </p>
        <p align="right" class="like_goon_btn"><a href="###" id="product_detail_goon">继续购物</a></p>
    </div>
    <div class="clear"></div>
    <img src="${domain_image}/images/closebtn_01.jpg" id="like_close_product_detail" alt="" />
</div>
<!--you_may_also_like_end-->
