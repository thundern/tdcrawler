<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="Pragma" content="No-cache"/>
		<meta http-equiv="Cache-control" content="no-cache"/>
		<meta http-equiv="Expires" content="0"/>
		<meta name="keywords" content="<fmt:message key="${meta_keywords}"></fmt:message>" />
		<meta name="description" content="<fmt:message key="${meta_description}"></fmt:message>" />

		<tiles:importAttribute name="title" toName="title_homepage" scope="request" />
		<title><fmt:message key="${title_homepage}"></fmt:message></title>
		<tiles:insertAttribute name="meta" />
	</head>
	
	<body id="<tiles:getAsString name='bodyid'/>" root="${pageContext.request.contextPath}">
		<tiles:insertAttribute name="header" />
		<!-- begin #container -->
		<div class="my_account_bg">
		   <div class="category my_wrap cf">
			 <tiles:insertAttribute name="menu" />
			 <tiles:insertAttribute name="content" />
		   </div>
		</div>
		<!-- end #container -->
	 
		<!-- begin #footer -->
		<tiles:insertAttribute name="footer" />
		<!-- end #footer -->
		
	</body>
</html>
