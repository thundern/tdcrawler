<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<tiles:importAttribute name="forbiddencache" toName="forbiddencache" scope="request" />
<c:if test="${forbiddencache == 'true'}">
	<meta http-equiv="Pragma" content="No-cache" />
	<meta http-equiv="Cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
</c:if>

<c:choose>
	<c:when test="${not empty meta_keywords && !fn:startsWith(meta_keywords,'meta.')}">
		<meta name="keywords" content="${meta_keywords}" />
	</c:when>
	<c:otherwise>
		<meta name="keywords" content="<fmt:message key="${meta_keywords}"></fmt:message>" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty meta_description  && !fn:startsWith(meta_description,'meta.')}">
		<meta name="description" content="${meta_description}" />
	</c:when>
	<c:otherwise>
		<meta name="description" content="<fmt:message key="${meta_description}"></fmt:message>" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${pageTitle != null}">
		<title>${pageTitle}</title>
	</c:when>
	<c:otherwise>
		<tiles:importAttribute name="title" toName="title_homepage" scope="request" />
		<title><fmt:message key="${title_homepage}"></fmt:message></title>
	</c:otherwise>
</c:choose>
<tiles:insertAttribute name="meta" />
</head>
<body id="<tiles:getAsString name='bodyid'/>" root="${pageContext.request.contextPath}">
	<!-- begin #header -->
	<tiles:insertAttribute name="header" />
	<!-- end #header -->

	<!--begin content-->
	<tiles:insertAttribute name="content" />
	<!-- end content -->

	<!--begin blog-->
	<tiles:insertAttribute name="blog" />
	<!-- end blog -->

	<!-- begin #footer -->
	<tiles:insertAttribute name="footer" />
	<!-- end #footer -->

	<%-- omnitureRef --%>
	<c:if test="${empty omnitureRef}">
		<script type="text/javascript">
			var omnitureRef = "";
		</script>
	</c:if>

	<c:if test="${not empty omnitureRef}">
		<%-- omnitureRef --%>
		<script type="text/javascript">
			var omnitureRef = ${omnitureRef};
		</script>
	</c:if>

</body>
</html>
