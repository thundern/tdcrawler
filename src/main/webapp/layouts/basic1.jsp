<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
<%--<link rel="dns-prefetch" href="//china.coach.com"/>--%>
<%--<link rel="dns-prefetch" href="//coach.scene7.com"/>--%>
<%--<link rel="dns-prefetch" href="//img.china.coach.com"/>--%>
<%-- 
<link rel="dns-prefetch" href="//hm.baidu.com"/>
<link rel="dns-prefetch" href="//cm.ipinyou.com"/>
<link rel="dns-prefetch" href="//c.x.cn.miaozhen.com"/>
<link rel="dns-prefetch" href="//cm.g.doubleclick.net"/>
--%>
<%--<link rel="dns-prefetch" href="//metrics.coach.com"/>--%>


<c:choose>
	<c:when test="${pageTitle != null}">
		<title>${pageTitle}</title>
	</c:when>
	<c:otherwise>
		<tiles:importAttribute name="title" toName="title_homepage" scope="request" />
		<title><fmt:message key="${title_homepage}"></fmt:message></title>
	</c:otherwise>
</c:choose>

<tiles:insertAttribute name="meta" />

<!-- google tracking code -->
<!-- <script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push([ '_setAccount', 'UA-18074968-1' ]);
	_gaq.push([ '_trackPageview' ]);

	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script> -->

</head>

<body id="<tiles:getAsString name='bodyid'/>" root="${pageContext.request.contextPath}" oncontextmenu="return false">

	<%--accuen code start --%>
	<script>
		dataLayer = [ {
			'event' : 'PageView'
		} ];
	</script>
	<noscript>
		<iframe src="//www.googletagmanager.com/ns.html?id=GTM-KD724Z"
			height="0" width="0" style="display: none; visibility: hidden"></iframe>
	</noscript>
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start' : new Date().getTime(),
				event : 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l='
					+ l
					: '';
			j.async = true;
			j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-KD724Z');
	</script>
	<%--accuen code end --%>
	
	<!-- begin #header -->
	<tiles:insertAttribute name="header" />
	<!-- end #header -->

	<!-- begin #mainContent -->
	<tiles:insertAttribute name="banner" />
	<tiles:insertAttribute name="content" />
	<!-- end #mainContent -->

	<!-- begin #footer -->
	<tiles:insertAttribute name="footer" />
	<!-- end #footer -->

	<!-- begin #popup layer -->
	<tiles:insertAttribute name="popup" />
	<!-- end #popup layer -->



	<%-- omnitureRef --%>
	<c:if test="${empty omnitureRef}">
		<script type="text/javascript">
			var omnitureRef = "";
		</script>
	</c:if>

	<c:if test="${not empty omnitureRef}">
		<%-- omnitureRef --%>
		<script type="text/javascript">
			var omnitureRef = ${omnitureRef};
		</script>
	</c:if>

	<!-- Live800在线客服图标:HTTPS首页图标代码[固定型] 开始-->
	
	<script id='write' language="javascript">
	function writehtml(){
		var iTop = (window.screen.availHeight-30-480)/2;
		var iLeft = (window.screen.availWidth-10-620)/2;
		var temptext="<a class='specialist navchat'  href='javascript:void(0)' target='_self' id='live800iconlink' onclick=\"window.open('http://care.live800.com/live800/chatClient/chatbox.jsp?companyID=223328&configID=114958&jid=5042586445";
		temptext += "&enterurl="+document.URL;
		temptext +="&timestamp="+Math.round(new Date().getTime());
		temptext +="&pagereferrer=";
		temptext += "&pagetitle="+document.title;
		temptext += "','','height=480, width=620, top="+iTop+",left="+iLeft+", toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no')\">";
		//temptext += "<img src='${domain_image }/images/chat_icon_pink.jpg'>";
		temptext += "</a>";
		if(document.getElementById('live114958')) {
			document.getElementById('live114958').innerHTML=temptext;
			setTimeout('write.src',9000);
		}
		if(document.getElementById('footerlive114958')) {
			document.getElementById('footerlive114958').innerHTML=temptext;
			setTimeout('write.src',9000);
		}		
	}
	writehtml();
	</script>
	
	<!-- Live800在线客服图标:HTTPS首页图标代码[固定型] 结束-->
	
	<!-- Live800默认跟踪代码: 开始>
	<script language="javascript" src="https://care.live800.com/live800/chatClient/monitor.js?jid=5042586445&companyID=223328&configID=114951&codeType=custom"></script>
	<Live800默认跟踪代码: 结束-->
		<!-- baidu tracking code
	<script type="text/javascript">
		var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
		document.write(unescape("%3Cscript src='" + _bdhmProtocol
				+ "hm.baidu.com/h.js%3F24c4f79fe60a2aa0a57f1d4fc0c46739' type='text/javascript'%3E%3C/script%3E"));
	</script> -->
	
	<!-- double click -->
	<%-- <div>
	<script>-function(d){var a=location.protocol,e=(a=="https:");d.write("<script src='"+(e?'https':'http')+"://file.ipinyou.com"+(e?"":".cn")+"/js/coach.js'><\/script>");}(document);</script>
	<noscript><img src="//optimus.ipinyou.com/statsadv?v=1&owId=3366&owLabelId=" style="display:none;"></noscript>
	</div>--%>
	

</body>
</html>