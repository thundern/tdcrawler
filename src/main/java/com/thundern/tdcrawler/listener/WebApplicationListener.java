package com.thundern.tdcrawler.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thundern.tdcrawler.service.schedule.CrawlerSchedule;

public class WebApplicationListener implements ServletContextListener{

	Logger logger = LoggerFactory.getLogger(getClass());
	
	public void contextInitialized(ServletContextEvent sce) {
		logger.error("start to init listener");
		System.out.println("start init listener");
		CrawlerSchedule cs = new CrawlerSchedule();
		cs.init();	
	}

	public void contextDestroyed(ServletContextEvent sce) {
		
	}



}
