//package com.thundern.tdcrawler.listener;
//
//import javax.management.JMException;
//import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import us.codecraft.webmagic.Spider;
//import us.codecraft.webmagic.monitor.SpiderMonitor;
//import us.codecraft.webmagic.samples.OschinaBlogPageProcesser;
//import us.codecraft.webmagic.scheduler.QueueScheduler;
//import us.codecraft.webmagic.scheduler.component.BloomFilterDuplicateRemover;
//
//public class CrawlerListener  implements ServletContextListener{
//
//	Logger logger = LoggerFactory.getLogger(getClass());
//	
//	public void contextInitialized(ServletContextEvent sce) {
//
//		 Spider spider = Spider.create(new OschinaBlogPageProcesser()).setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(2000)));
//	        try {
//				SpiderMonitor.instance().register(spider);
//			} catch (JMException e) {
//				logger.error(e.toString());
//			}
//	        spider.run();		
//	}
//
//	public void contextDestroyed(ServletContextEvent sce) {
//		
//	}
//
//}
