package com.thundern.tdcrawler.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.thundern.tdcrawler.model.Site;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��3�� ����11:35:59
 * @description TODO
 */
public class SpiderListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent sce) {
				
		ApplicationContext wac =  WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
		
		Site site =	(Site) wac.getBean("oschinaBlogSite");
		
//		wac.getBeansOfType(type);
		
		Job job = (Job) wac.getBean("oschinaBlogSpiderJob");
		job.doJob();
		
	}

	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
