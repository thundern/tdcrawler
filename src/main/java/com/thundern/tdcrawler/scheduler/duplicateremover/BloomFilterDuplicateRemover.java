package com.thundern.tdcrawler.scheduler.duplicateremover;

import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����4:37:53
 * @description TODO
 */
public class BloomFilterDuplicateRemover implements DuplicateRemover {

	private int expectedInsertions;
	
	private double fpp;
	
	private AtomicInteger counter;
	
	public BloomFilterDuplicateRemover(int expectedInsertions) {
		this(expectedInsertions, 0.01);
	}
	
	
	public BloomFilterDuplicateRemover(int expectedInsertions , double fpp) {
		this.expectedInsertions = expectedInsertions;
		this.fpp = fpp;
		bloomFilter = rebuildBloomFilter();		
	}

	private BloomFilter<CharSequence> rebuildBloomFilter() {
		this.counter = new AtomicInteger(0);
		return BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), expectedInsertions);
	}
	
	private final BloomFilter<CharSequence> bloomFilter;
	
	public boolean isDuplicate(Request request, Job job) {
		boolean isDuplicate = bloomFilter.mightContain(getUrl(request));
		if(!isDuplicate) {
			bloomFilter.put(getUrl(request));
			counter.incrementAndGet();
		}
		return isDuplicate;
	}
	private String getUrl(Request request) {
		return request.getUrl();
	}
	
	public void resetDuplicateCheck(Job job) {
		rebuildBloomFilter();		
	}

	public int getTotalRequestsCount(Job job) {		
		return counter.get();
	}

}
