package com.thundern.tdcrawler.scheduler.duplicateremover;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.Sets;
import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����5:46:23
 * @description TODO
 */
public class HashSetDuplicateRemover implements DuplicateRemover {

	private Set<String> urls = Sets.newSetFromMap(new ConcurrentHashMap<String, Boolean>() ); 
	
	
	public boolean isDuplicate(Request request, Job job) {
		return !urls.add(getUrl(request));		
	}
	
	protected String getUrl(Request request) {
        return request.getUrl();
    }

	public void resetDuplicateCheck(Job job) {
		urls.clear();
	}

	public int getTotalRequestsCount(Job job) {
		return urls.size();
	}

}
