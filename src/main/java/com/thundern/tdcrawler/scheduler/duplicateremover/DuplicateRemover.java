package com.thundern.tdcrawler.scheduler.duplicateremover;

import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����4:37:21
 * @description TODO
 */
public interface DuplicateRemover {

	public boolean isDuplicate(Request request, Job job);
	
	public void resetDuplicateCheck(Job job);
	
	public int getTotalRequestsCount(Job job);
		
}
