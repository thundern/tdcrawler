package com.thundern.tdcrawler.scheduler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;

import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015年11月2日 下午4:33:01
 * @description TODO
 */
public class QueueScheduler extends DuplicateRemovedScheduler implements MonitorableScheduler {

	private BlockingQueue<Request> queue /*= new LinkedBlockingQueue<Request>()*/;
		
	//DuplicateRemovedScheduler中实现 调用pushWhenNoDuplicate()..
	/*public void push(Request request, Job job) {
		// TODO Auto-generated method stub
		
	}
	 */
	
	@PostConstruct
	public void initBean() {
		queue = new LinkedBlockingQueue<Request>();
	}
	
	@Override
	public void pushWhenNoDuplicate(Request request , Job job) {
		queue.add(request);
	}
	
	public Request poll(Job job) {
		return queue.poll();
	}

	public int getLeftRequestsCount(Job job) {
		return queue.size();
	}

	public int getTotalRequestsCount(Job job) {
		return getDuplicatedRemover().getTotalRequestsCount(job);
	}

}
