package com.thundern.tdcrawler.scheduler;

import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����6:03:43
 * @description TODO
 */
public interface MonitorableScheduler extends Scheduler{

	public int getLeftRequestsCount(Job job);
	
	public int getTotalRequestsCount(Job job);
}
