package com.thundern.tdcrawler.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.scheduler.duplicateremover.DuplicateRemover;
import com.thundern.tdcrawler.scheduler.duplicateremover.HashSetDuplicateRemover;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����5:36:00
 * @description TODO
 */
public abstract class DuplicateRemovedScheduler implements Scheduler {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private DuplicateRemover duplicatedRemover = new HashSetDuplicateRemover();

	public DuplicateRemover getDuplicatedRemover() {
		return duplicatedRemover;
	}

	public DuplicateRemover setDuplicatedRemover(DuplicateRemover duplicatedRemover) {
		this.duplicatedRemover = duplicatedRemover;
		return this.duplicatedRemover;
	}
	
	public void push(Request request, Job job) {
		logger.debug("get a candidate url [{}].", request.getUrl());
		if( !duplicatedRemover.isDuplicate(request, job) || shouldReserved(request) ) {
			logger.info("push a url to queue[{}]",request.getUrl());
			pushWhenNoDuplicate(request, job);
		}
	}
	
	protected boolean shouldReserved(Request request) {
		return request.getExtra(Request.CYCLE_TRIED_TIMES) != null;		
	}
	
	protected void pushWhenNoDuplicate(Request request, Job job) {
		
	}
	
}
