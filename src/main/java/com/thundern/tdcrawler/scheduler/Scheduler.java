package com.thundern.tdcrawler.scheduler;

import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����4:32:24
 * @description TODO
 */
public interface Scheduler {

	public void push(Request request, Job job);
	
	
	public Request poll(Job job);
	
}
