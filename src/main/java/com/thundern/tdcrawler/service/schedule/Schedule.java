package com.thundern.tdcrawler.service.schedule;

import java.io.Serializable;

public interface Schedule extends Serializable {

	public void init();
	
	public void destroy();
	
}
