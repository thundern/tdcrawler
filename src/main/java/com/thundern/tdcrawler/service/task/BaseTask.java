package com.thundern.tdcrawler.service.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseTask implements Task{
	public Logger logger = LoggerFactory.getLogger(getClass());
	
	public void init(){}
	
	public void destroy(){}
}
