package com.thundern.tdcrawler.service.task.pipeline;

import com.thundern.tdcrawler.model.ResultItems;
import com.thundern.tdcrawler.service.job.Job;
import com.thundern.tdcrawler.service.task.Task;

/**
 * @author sid
 * @version 2015��11��2�� ����11:51:35
 * @description TODO
 */
public interface Pipeline {

    public void process(ResultItems resultItems, Job task);
}
