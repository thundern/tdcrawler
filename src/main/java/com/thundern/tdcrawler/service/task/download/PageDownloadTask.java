package com.thundern.tdcrawler.service.task.download;

import com.thundern.tdcrawler.model.Page;
import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.service.job.Job;

/**
 * @author sid
 * @version 2015��11��2�� ����11:58:05
 * @description TODO
 */
public interface PageDownloadTask {
	public Page download(Request request ,Job job) ;
	public void setThread(int threadNum);
}
