package com.thundern.tdcrawler.service.task.download;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thundern.tdcrawler.model.Page;
import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.model.Site;
import com.thundern.tdcrawler.service.job.Job;
import com.thundern.tdcrawler.service.task.BaseTask;

/**
 * @author sid
 * @version 2015��11��2�� ����11:59:02
 * @description TODO
 */
public abstract class AbstractPageDownload extends BaseTask implements PageDownloadTask {

	Logger logger = LoggerFactory.getLogger(getClass());
	
	public Page download(Request request, Job job) {
		return null;
	}
	
	protected void onSuccess(Request request) {
		
		logger.info("success for fetch url :\r\n {}", request.getUrl());
	}

    protected void onError(Request request) {
    }
    protected Page addToCycleRetry(Request request, Site site) {
        Page page = new Page();
        Object cycleTriedTimesObject = request.getExtra(Request.CYCLE_TRIED_TIMES);
        if (cycleTriedTimesObject == null) {
            page.addTargetRequest(request.setPriority(0).putExtra(Request.CYCLE_TRIED_TIMES, 1));
        } else {
            int cycleTriedTimes = (Integer) cycleTriedTimesObject;
            cycleTriedTimes++;
            if (cycleTriedTimes >= site.getCycleRetryTimes()) {
                return null;
            }
            page.addTargetRequest(request.setPriority(0).putExtra(Request.CYCLE_TRIED_TIMES, cycleTriedTimes));
        }
        page.setNeedCycleRetry(true);
        return page;
    }
}
