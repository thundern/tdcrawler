package com.thundern.tdcrawler.service.task.pageparser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thundern.tdcrawler.model.Page;
import com.thundern.tdcrawler.service.task.BaseTask;

/**
 * @author sid
 * @version 2015��11��2�� ����11:41:40
 * @description TODO
 */
public abstract class AbstractPageParser extends BaseTask implements PageParserTask {

	public Logger logger = LoggerFactory.getLogger(getClass());
	
	public abstract void parsePage(Page page) throws NullPointerException;
	
	
	
}
