package com.thundern.tdcrawler.service.task.pipeline;

import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.thundern.tdcrawler.model.ResultItems;
import com.thundern.tdcrawler.service.job.Job;
import com.thundern.tdcrawler.service.task.Task;

/**
 * @author sid
 * @version 2015��11��2�� ����11:52:25
 * @description TODO
 */
@Component("consolePipeline")
public class ConsolePipeline extends AbstractPipeline implements Pipeline {
	public void process(ResultItems resultItems, Job task) {
		logger.info("Pipeline page url :{}",resultItems.getRequest().getUrl());		
//		System.out.println("get page: " + resultItems.getRequest().getUrl());
        for (Entry<String, Object> entry : resultItems.getAll().entrySet()) {
            logger.info(entry.getKey() + " : " + entry.getValue());	
        }
	}
}
