package com.thundern.tdcrawler.service.task.download;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpClientConnection;
import org.apache.http.HttpConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thundern.tdcrawler.model.Page;


@SuppressWarnings("deprecation")
public class RequestUtil {
	
	static Logger logger = LoggerFactory.getLogger(RequestUtil.class);
	
    public static Page visitUrl( String srcUrl) {
    	
    	CloseableHttpClient  httpClient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(srcUrl);
		UrlEncodedFormEntity uefEntity= initParams();
		httpPost.setEntity(uefEntity);
		httpPost.setHeaders( initHeader() );
		CloseableHttpResponse  httpResponse = null;
    	HttpEntity httpEntity = null;
    	String result = null;
    	    	
    	try {
    		httpResponse = httpClient.execute(httpPost);
    		int resultCode =  httpResponse.getStatusLine().getStatusCode();
    		logger.info("request with srcUrl is return [{}]" , resultCode );
    		if( httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK ) {
    			httpEntity = httpResponse.getEntity();
    			result = EntityUtils.toString(httpEntity);    			
    		}
    		
    		EntityUtils.consume(httpEntity);
    		
		} catch (ClientProtocolException e) {
			logger.error("error is [{}] . " , e.toString());
		} catch (IOException e) {
			logger.error("error is [{}] . " , e.toString());
		}
    	finally {
    		httpClient.getConnectionManager().shutdown();
    		logger.info("finally shutdown!!");
    		
		}
    	
    	Page page = new Page();
    	page.setRawText(result);
    	
		return page;
		
	}
    
    private static UrlEncodedFormEntity initParams(){
    	List<NameValuePair> requestParams = new ArrayList<NameValuePair>();
    	
//    	requestParams.add( new BasicNameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36")  );
//    	requestParams.add( new BasicNameValuePair("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")  );
////    	:
//    	requestParams.add( new BasicNameValuePair("Accept-Encoding", "gzip, deflate, sdch")  );
////    	:
//    	requestParams.add( new BasicNameValuePair("Accept-Language", "zh-CN,zh;q=0.8")  );
////    	:
//    	requestParams.add( new BasicNameValuePair("Cache-Control", "no-cache")  );
////    	Connection:keep-alive
////    	Host:www.oschina.net
//    	requestParams.add( new BasicNameValuePair("Pragma", "no-cache")  );
//    	requestParams.add( new BasicNameValuePair("Upgrade-Insecure-Requests", "1")  );
    	UrlEncodedFormEntity urlEncodedFormEntity = null;
    	try {
			urlEncodedFormEntity = new UrlEncodedFormEntity(requestParams,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
    	
    	
    	return urlEncodedFormEntity;
    	
    	
    }
	
    
    private static Header[] initHeader(){
    	
    	List<Header> headers = new ArrayList<Header>();
    	
    	headers.add( new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"));
    	
    	Header[] hs = new Header[headers.size()];
		return headers.toArray(hs);
		
    
    
    
    
    
    }
}
