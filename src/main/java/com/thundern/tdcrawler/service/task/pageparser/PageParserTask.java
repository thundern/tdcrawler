package com.thundern.tdcrawler.service.task.pageparser;

import com.thundern.tdcrawler.model.Page;
import com.thundern.tdcrawler.model.Site;
import com.thundern.tdcrawler.service.task.Task;

/**
 * @author sid
 * @version 2015��11��2�� ����11:38:37
 * @description TODO
 */
public interface PageParserTask extends Task {

	public void parsePage(Page page) throws NullPointerException;
	
	public Site getSite();		
}
