package com.thundern.tdcrawler.service.job;

import com.thundern.tdcrawler.model.Site;

public interface Job {

	public void doJob();
	
	//current is starturl
	public String getUUID();

	public Site getSite();
}
