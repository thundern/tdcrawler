package com.thundern.tdcrawler.service.job;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpHost;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.thundern.tdcrawler.executor.CountableThreadPool;
import com.thundern.tdcrawler.model.Page;
import com.thundern.tdcrawler.model.Request;
import com.thundern.tdcrawler.model.Site;
import com.thundern.tdcrawler.scheduler.Scheduler;
import com.thundern.tdcrawler.service.task.download.PageDownloadTask;
import com.thundern.tdcrawler.service.task.pageparser.PageParserTask;
import com.thundern.tdcrawler.service.task.pipeline.Pipeline;
import com.thundern.tdcrawler.urls.UrlUtils;

/**
 * @author sid
 * @version 2015��11��3�� ����10:58:42
 * @description TODO
 */
@Component("oschinaBlogSpiderJob")
public class OschinaBlogSpiderJob extends SpiderJob implements InitializingBean{

	@Autowired()
	@Qualifier(value="oschinaBlogSite")
	private Site site;

	@Autowired()
	@Qualifier(value="queueScheduler")
	private Scheduler scheduler;
	
	@Autowired()
	@Qualifier(value="pageDownload")
	private PageDownloadTask downloadTask;
	
	@Autowired()
	@Qualifier(value="oschinaBlogPageParser")
	private PageParserTask pageParserTask;
		
	@Autowired
	private List<Pipeline> pipelines;
//	@Qualifier(value="")
	private Request startRequest;

	private boolean spawnUrl;

	
//	private CountableThreadPool threadPool;
	
	public CountableThreadPool getThreadPool() {
		return threadPool;
	}
	
	@Autowired
	public void setThreadPool(CountableThreadPool threadPool) {
		this.threadPool = threadPool;
	}

	public void afterPropertiesSet() throws Exception {
		startRequest = site.getStartRequests().get(0);		
		spawnUrl = true;
		logger.info("first url is [{}] ", startRequest.getUrl() );
		scheduler.push(startRequest, this);		
	}
	
	public OschinaBlogSpiderJob(){		
		super();
	}

	@Override
	public void doJob() {
//		checkRunningStat();
//      initComponent();
        logger.info("Spider " + getUUID() + " started!");
        while (!Thread.currentThread().isInterrupted() && stat.get() == STAT_RUNNING) {
            Request request = scheduler.poll(this);
            if (request == null) {
                if (threadPool.getAliveThread() == 0 && exitWhenComplete) {
                    break;
                }
                // wait until new url added
                waitNewUrl();
            } else {
                final Request requestFinal = request;
                threadPool.execute2(new Runnable() {
//                    @Override
                    public void run() {
                        try {
                        	doService(requestFinal);
                            onSuccess(requestFinal);
                            pageCount.incrementAndGet();
                        } catch (Exception e) {
                            onError(requestFinal);
                            logger.error("process request " + requestFinal + " error", e);
                        } finally {
                            if (site.getHttpProxyPool().isEnable()) {
                                site.returnHttpProxyToPool((HttpHost) requestFinal.getExtra(Request.PROXY), ((Integer) requestFinal
                                        .getExtra(Request.STATUS_CODE)).intValue() );
                            }
//                            pageCount.incrementAndGet();
                            signalNewUrl();
                        }
                    }
                });
            }
        }
        stat.set(STAT_STOPPED);
        // release some resources
        if (destroyWhenExit) {
//            close();
        	logger.info("finish all the request.& count of req is {} ", pageCount.get());
            System.exit(0);
        }
		
		
		
		
//		doService();
	}
	
	private void doService(Request request) {
//		Page page = downloadTask.download(startRequest, this);
		Page page = downloadTask.download(request, this);
		pageParserTask.parsePage(page);
		
		extractAndAddRequests(page, spawnUrl);
		
		for(Pipeline pipeline : pipelines) {
			pipeline.process(page.getResultItems(), this);
//			logger.info("pipeline");
		}
	}
	
	protected void extractAndAddRequests(Page page, boolean spawnUrl) {
        if (spawnUrl && CollectionUtils.isNotEmpty(page.getTargetRequests())) {
            for (Request request : page.getTargetRequests()) {
                addRequest(request);
            }
        }        
        logger.info("Current page size is {} ." , page.getTargetRequests().size());        
    }

    private void addRequest(Request request) {
        if (site.getDomain() == null && request != null && request.getUrl() != null) {
            site.setDomain(UrlUtils.getDomain(request.getUrl()));
        }
        scheduler.push(request, this);
    }
    
	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getUUID() {
		// TODO Auto-generated method stub
		return site.getDomain();
	}

}
