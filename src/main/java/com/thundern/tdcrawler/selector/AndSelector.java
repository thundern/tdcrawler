//
//
//  Generated by StarUML(tm) Java Add-In
//
//  @ Project : TdCrawler
//  @ File Name : AndSelector.java
//  @ Date : 2015/10/9
//  @ Author : 
//
//



package com.thundern.tdcrawler.selector;

import java.util.ArrayList;
import java.util.List;

public class AndSelector implements Selector {
	
	private List<Selector> selectors = new ArrayList<Selector>();
	
	public AndSelector(Selector... selectors) {
		for( Selector selector : selectors) {
			this.selectors.add(selector);
		}
	}
	
	public AndSelector(List<Selector> selectors)  {
		this.selectors = selectors;
	}
	
	public String select(String text){
		if(text == null) return null; 
		
		for(Selector selector : selectors)
			text = selector.select(text);
		
		return text;
	}
	
	public List<String> selectList(String text){
		List<String> results = new ArrayList<String>();
		boolean first = true;
		for(Selector selector : selectors ) {
			if (first) {
				results = selector.selectList(text);
				first = false;				
			} else {
				List<String> resultsTemp = new ArrayList<String>();
				for(String result : results) {
					resultsTemp.addAll(selector.selectList(result));
				}
				results = resultsTemp;
				if( results==null || results.size()==0 ) {
					return results;
				}
			}
		}
		return results;
	}
	
	
}
