package com.thundern.tdcrawler.selector;

class RegexResult {

	private String[] groups;
	
	public static final RegexResult EMPTY_REGEXRESULT = new RegexResult();
	
	public RegexResult(){}
	
	public RegexResult(String[] groups){
		this.groups = groups;
	}
	
	public String get(int groupId) {		
		return groups==null ? null : groups[groupId];
	}
	
}
