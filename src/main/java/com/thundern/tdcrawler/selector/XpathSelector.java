
package com.thundern.tdcrawler.selector;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.nodes.Element;

import us.codecraft.xsoup.XPathEvaluator;
import us.codecraft.xsoup.Xsoup;

public class XpathSelector extends BaseElementSelector {
	
	private XPathEvaluator xPathEvaluator; 
	
	public XpathSelector(String xpathStr) {
		this.xPathEvaluator = Xsoup.compile(xpathStr);
	}

	public String select(Element element) {
		return xPathEvaluator.evaluate(element).get();
	}
	
	public List<String> selectList(Element element) {
		return xPathEvaluator.evaluate(element).list();
	}
	
	public Element selectElement(Element element) {
		List<Element> elements = new ArrayList<Element>();		
		if(CollectionUtils.isNotEmpty(elements)) {
			return elements.get(0);
		}		
		return null;
	}
	
	public List<Element> selectElements(Element element) {		
		return xPathEvaluator.evaluate(element).getElements();
	}
	
	@Override
	public boolean hasAttribute() {		
		return xPathEvaluator.hasAttribute();
	}
	
}
