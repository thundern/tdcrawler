
package com.thundern.tdcrawler.selector;

import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.JsonPath;

public class JsonPathSelector implements Selector {
	
	private String jsonPathStr;
	private JsonPath jsonPath;
	
	public JsonPathSelector(String jsonPathStr) {
		this.jsonPathStr = jsonPathStr;
		this.jsonPath = JsonPath.compile(this.jsonPathStr);
	}
	
	public String select(String text){
		Object object = jsonPath.read(text);
		if(object == null ) return null;
		if(object instanceof List) {
			List list = (List)object;
			if(list != null && list.size()>0) {
				return list.iterator().next().toString();
			}
		}		
		return object.toString();
	}
	
	public List<String> selectList(String text){
		List<String> list = new ArrayList<String>();
		Object object = jsonPath.read(text);
		if(object == null) {
			return list;
		}
		
		if(object instanceof List) {
			List<Object> items = (List<Object>)object;
			for(Object item : items) {
				list.add(String.valueOf(item));
			}
		}else {
			list.add(object.toString());
		}
		return list;
	}
		
}
