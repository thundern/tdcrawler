package com.thundern.tdcrawler.executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;

/**
 * @author sid
 * @version 2015年11月2日 下午6:37:53
 * @description TODO
 */

//已在xml中配置threadPool
public class CountableThreadPool {

	Logger logger = LoggerFactory.getLogger(getClass());

	
	private AtomicInteger threadAlive = new AtomicInteger();
	
	private ReentrantLock reentrantLock = new ReentrantLock();
	
	private Condition condition = reentrantLock.newCondition();
	
//	public ExecutorService executorService = Executors.newFixedThreadPool(numThread);
	
	private int numThread/*=10*/;
//	@Autowired
//	@Qualifier("springTaskExecutor")
	public TaskExecutor taskExecutor;
	
//	public Executor executorService;
	@PostConstruct
	public void init() {
		logger.info("CountableThreadPool initialed!");
		threadAlive = new AtomicInteger();		
		reentrantLock = new ReentrantLock();		
		condition = reentrantLock.newCondition();
	}
	
	public CountableThreadPool(){}
	
	public CountableThreadPool(int numThread) {
		this.numThread = numThread;
//		this.executorService = Executors.newFixedThreadPool(numThread);		
	}
	
	/*public CountableThreadPool(int numThread, ExecutorService executorService) {
		this.numThread = numThread;
		this.executorService = executorService;
	}*/
	
	/*public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;		
	}*/
	
	public int getAliveThread() {
		return threadAlive.get();
	}
	
//	public int getThreadNum() {
//		return this.numThread;		
//	}
	
	public int getNumThread() {
		return this.numThread;
	}

	public void setNumThread(int numThread) {
		this.numThread = numThread;
	}

	public TaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	public void setTaskExecutor(TaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}
	//使用 spring的executorService
	/*public void execute(final Runnable runnable) {
		if(threadAlive.get() >= numThread) {
			try {
				reentrantLock.lock();
				while(threadAlive.get() >= numThread) {
					try {
						condition.await();
					} catch (Exception e) {
					}
				}
				
			} finally {
				reentrantLock.unlock();
			}
		}
		
		threadAlive.incrementAndGet();
		
		executorService.execute(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} finally {
					try {
						reentrantLock.lock();
						threadAlive.decrementAndGet();
						condition.signal();					
					} finally {
						reentrantLock.unlock();
					}
				}
			}
		});
	}*/
	

	public void execute2(final Runnable runnable) {
		if(threadAlive.get() >= numThread) {
			try {
				reentrantLock.lock();
				while(threadAlive.get() >= numThread) {
					try {
						condition.await();
					} catch (Exception e) {
					}
				}
				
			} finally {
				reentrantLock.unlock();
			}
		}
		
		threadAlive.incrementAndGet();
		
		taskExecutor.execute(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} finally {
					try {
						reentrantLock.lock();
						threadAlive.decrementAndGet();
						condition.signal();					
					} finally {
						reentrantLock.unlock();
					}
				}
			}
		});
	}
	
	
	/*public boolean isShutdown() {
		return executorService.isShutdown();
	}*/
	
	/*public void shutdown() {
		executorService.shutdown();
	}*/

	
}
