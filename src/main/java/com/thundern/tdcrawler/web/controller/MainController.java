package com.thundern.tdcrawler.web.controller;

import com.thundern.tdcrawler.web.model.Menu;
import com.thundern.tdcrawler.web.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.RequestWrapper;
import java.util.List;

/**
 * Created by Administrator on 2015/11/17.
 */

@Controller
public class MainController extends  AbstractBaseController {

    @Autowired
    public MenuService menuService;

    @RequestMapping(value = {"/","/mgt/index.htm","/main.htm"},method= RequestMethod.GET)
    public  String welcome(HttpServletRequest request, HttpServletResponse response, Model model) {
        System.out.println("Main Main Main Main Main Main Main ");
        return  "welcome";
    }

    @RequestMapping(value = {"/index.htm"},method= RequestMethod.GET)
    public  String index(HttpServletRequest request, HttpServletResponse response, Model model) {
        System.out.println("Main Main Main Main Main Main Main ");

        Menu menu = menuService.getById(new Long(1));

//        Menu menu2 = menuService.getByHQL("select * from t_sys_menu");
        List<Menu> m2 = menuService.getByHQL("from Menu");

        if( m2==null || m2.size()==0) {
            System.out.println("m2 is null");
        }else {
            System.out.println("m2.size is " + m2.size()) ;
        }
        model.addAttribute("m",menu);
        model.addAttribute("m2",m2.get(0));
        return  "welcome";
    }

}
