package com.thundern.tdcrawler.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.XmlViewResolver;

@Controller
public class DemoController extends AbstractBaseController {

//	@RequestMapping(value="/demo/randomstring.json")
	public String demo(HttpServletRequest request, HttpServletResponse response, ModelMap model,
			@RequestParam("dataJson") String dataJson ,@RequestParam("dataString") String dataString) {
		
//		XmlViewResolver
		
		String dataJson2 = request.getParameter("dataJson");
		String dataString2 = request.getParameter("dataString");
		
		
//		model.addAttribute("dataJson1", dataJson);
//		model.addAttribute("dataJson2", dataJson2);
//		
//		System.out.println("dataJson1 is " + dataJson);
//		System.out.println("dataJson2 is " + dataJson2);
//		
//		System.out.println("dataString is " + dataString);
//		System.out.println("dataString2 is " + dataString2);
		model.addAttribute("dataString",dataString);
//		logger.info("dataString is {}", dataString);
		
		
		return "json";
	}


}
