package com.thundern.tdcrawler.web.model;

import javax.persistence.*;

/**
 * Created by Administrator on 2015/11/17.
 */
@Entity
@Table(name = "t_sys_menu")
public class Menu extends BaseModel{

    private Long id;
    private String name;
    private String url;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
