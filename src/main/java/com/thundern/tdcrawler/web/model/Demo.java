package com.thundern.tdcrawler.web.model;

import javax.persistence.*;

/**
 * Created by Administrator on 2015/11/18.
 */
@Entity
@Table(name = "t_sys_demo")
public class Demo extends BaseModel {

    private Long id;


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
