package com.thundern.tdcrawler.web.service;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/11/17.
 */
public interface BaseService<T> {

    public void save(T entity);

    public void update(T entity);

    public void delete(Serializable id);

    public T getById(Serializable id);

    public List<T> getByHQL(String hql, Object... params);
}
