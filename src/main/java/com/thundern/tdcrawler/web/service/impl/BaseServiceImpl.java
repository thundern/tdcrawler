package com.thundern.tdcrawler.web.service.impl;

import com.thundern.tdcrawler.web.dao.BaseDao;
import com.thundern.tdcrawler.web.service.BaseService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/11/17.
 */
@Transactional
public abstract class BaseServiceImpl<T> implements BaseService<T> {
    /**
     * 注入BaseDao
     */
    private BaseDao<T> dao;

    @Resource
    public void setDao(BaseDao<T> dao) {
        this.dao = dao;
    }

    public void save(T entity) {
        dao.save(entity);
    }

    public void update(T entity) {
        dao.update(entity);
    }

    public void delete(Serializable id) {
        dao.delete(id);
    }

    public T getById(Serializable id) {
        return dao.findById(id);
    }

    public List<T> getByHQL(String hql, Object... params) {
        return dao.findByHQL(hql, params);
    }


}
