package com.thundern.tdcrawler.web.service.impl;

import com.thundern.tdcrawler.web.dao.BaseDao;
import com.thundern.tdcrawler.web.dao.MenuDao;
import com.thundern.tdcrawler.web.model.Menu;
import com.thundern.tdcrawler.web.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2015/11/17.
 */
@Service("menuService")
public class MenuServiceImpl extends BaseServiceImpl<Menu> implements MenuService {

//    @Autowired
//    public MenuDao menuDao;

    @Resource(name = "menuDao")
    public void setDao(BaseDao<Menu> dao) {
        super.setDao(dao);
    }



}
