package com.thundern.tdcrawler.web.dao;

import com.thundern.tdcrawler.web.model.Menu;
import loxia.dao.GenericEntityDao;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2015/11/17.
 */

public interface MenuDao extends BaseDao<Menu>{
}
