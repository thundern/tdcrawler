package com.thundern.tdcrawler.web.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/11/17.
 */
public interface BaseDao<T> {
    public void save(T entity);

    public void update(T entity);

    public void delete(Serializable id);

    public T findById(Serializable id);

    public List<T> findByHQL(String hql, Object... params);
}
