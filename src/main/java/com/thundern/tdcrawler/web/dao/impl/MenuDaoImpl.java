package com.thundern.tdcrawler.web.dao.impl;

import com.thundern.tdcrawler.web.dao.BaseDaoImpl;
import com.thundern.tdcrawler.web.dao.MenuDao;
import com.thundern.tdcrawler.web.model.Menu;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2015/11/17.
 */
@Repository(value = "menuDao")
public class MenuDaoImpl extends BaseDaoImpl<Menu> implements MenuDao {


}
