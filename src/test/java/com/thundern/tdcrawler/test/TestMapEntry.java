package com.thundern.tdcrawler.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TestMapEntry {

	public static void main(String[] args) {
		Map<String, String> testMap = new HashMap<String, String>();
		
		testMap.put("a", "ni");
		System.out.println(testMap.get("a"));
		
		testMap.put("a", "wo");
		System.out.println(testMap.get("a"));
		
		Set<Entry<String, String>> testSet = testMap.entrySet();
		
		System.out.println(testSet.size());
	}
	
}
